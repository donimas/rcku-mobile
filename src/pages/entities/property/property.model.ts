import { BaseEntity } from './../../../models';

export class Property implements BaseEntity {
    constructor(
        public id?: number,
        public personal_account?: string,
        public last_payment?: string,
        public building_index?: string,
        public living_area?: string,
        public street?: string,
        public street_number?: string,
        public property_number?: string,
        // server params:
        public personalAccount?: string,
        public lastPayment?: string,
    ) {
    }
}
