import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { Property } from './property.model';

@Injectable()
export class PropertyService {
    private resourceUrl = Api.API_URL.replace('api', 'condo/api') + '/properties';

    constructor(private http: HttpClient) { }

    create(property: Property): Observable<Property> {
        return this.http.post(this.resourceUrl, property);
    }

    update(property: Property): Observable<Property> {
        return this.http.put(this.resourceUrl, property);
    }

    find(id: number): Observable<Property> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    forCurrentUser(req?: any): Observable<any> {
      return this.http.get(Api.API_URL.replace('api', 'condo/api') + '/properties-ext/forCurrentUser');
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
