import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { Property } from './property.model';
import { PropertyService } from './property.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-property',
    templateUrl: 'property.html'
})
export class PropertyPage {
    properties: Property[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private propertyService: PropertyService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.properties = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.propertyService.query().subscribe(
            (response) => {
                this.properties = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Property) {
        return item.id;
    }

    open(slidingItem: any, item: Property) {
        let modal = this.modalCtrl.create('PropertyDialogPage', {item: item});
        modal.onDidDismiss(property => {
            if (property) {
                if (property.id) {
                    this.propertyService.update(property).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'Property updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.propertyService.create(property).subscribe(data => {
                        this.properties.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'Property added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(property) {
        this.propertyService.delete(property.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'Property deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(property: Property) {
        this.navCtrl.push('PropertyDetailPage', {id: property.id});
    }
}
