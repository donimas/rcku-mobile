import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Property } from './property.model';
import { PropertyService } from './property.provider';

@IonicPage()
@Component({
    selector: 'page-property-dialog',
    templateUrl: 'property-dialog.html'
})
export class PropertyDialogPage {

    property: Property;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private propertyService: PropertyService) {
        this.property = params.get('item');
        if (this.property && this.property.id) {
            this.propertyService.find(this.property.id).subscribe(data => {
                this.property = data;
            });
        } else {
            this.property = new Property();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.property.id : null],
            personal_account: [params.get('item') ? this.property.personal_account : '',  Validators.required],
            last_payment: [params.get('item') ? this.property.last_payment : '',  Validators.required],
            /*building_index: [params.get('item') ? this.property.building_index : '', ],
            living_area: [params.get('item') ? this.property.living_area : '',  Validators.required],
            street: [params.get('item') ? this.property.street : '',  Validators.required],
            street_number: [params.get('item') ? this.property.street_number : '',  Validators.required],
            property_number: [params.get('item') ? this.property.property_number : '',  Validators.required],*/
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the property, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

}
