import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PropertyPage } from './property';
import { PropertyService } from './property.provider';

@NgModule({
    declarations: [
        PropertyPage
    ],
    imports: [
        IonicPageModule.forChild(PropertyPage),
        TranslateModule.forChild()
    ],
    exports: [
        PropertyPage
    ],
    providers: [PropertyService]
})
export class PropertyPageModule {
}
