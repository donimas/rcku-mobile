import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PropertyDialogPage } from './property-dialog';
import { PropertyService } from './property.provider';

@NgModule({
    declarations: [
        PropertyDialogPage
    ],
    imports: [
        IonicPageModule.forChild(PropertyDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        PropertyDialogPage
    ],
    providers: [
        PropertyService
    ]
})
export class PropertyDialogPageModule {
}
