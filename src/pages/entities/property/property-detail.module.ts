import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { PropertyDetailPage } from './property-detail';
import { PropertyService } from './property.provider';

@NgModule({
    declarations: [
        PropertyDetailPage
    ],
    imports: [
        IonicPageModule.forChild(PropertyDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        PropertyDetailPage
    ],
    providers: [PropertyService]
})
export class PropertyDetailPageModule {
}
