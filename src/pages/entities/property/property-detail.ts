import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { Property } from './property.model';
import { PropertyService } from './property.provider';

@IonicPage({
    segment: 'property-detail/:id',
    defaultHistory: ['EntityPage', 'propertyPage']
})
@Component({
    selector: 'page-property-detail',
    templateUrl: 'property-detail.html'
})
export class PropertyDetailPage {
    property: Property;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private propertyService: PropertyService, private toastCtrl: ToastController) {
        this.property = new Property();
        this.property.id = params.get('id');
    }

    ionViewDidLoad() {
        this.propertyService.find(this.property.id).subscribe(data => this.property = data);
    }

    open(item: Property) {
        let modal = this.modalCtrl.create('PropertyDialogPage', {item: item});
        modal.onDidDismiss(property => {
            if (property) {
                this.propertyService.update(property).subscribe(data => {
                    this.property = data;
                    let toast = this.toastCtrl.create(
                        {message: 'Property updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
