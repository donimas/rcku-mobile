import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { RegionDialogPage } from './region-dialog';
import { RegionService } from './region.provider';

@NgModule({
    declarations: [
        RegionDialogPage
    ],
    imports: [
        IonicPageModule.forChild(RegionDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        RegionDialogPage
    ],
    providers: [
        RegionService
    ]
})
export class RegionDialogPageModule {
}
