import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { RegionPage } from './region';
import { RegionService } from './region.provider';

@NgModule({
    declarations: [
        RegionPage
    ],
    imports: [
        IonicPageModule.forChild(RegionPage),
        TranslateModule.forChild()
    ],
    exports: [
        RegionPage
    ],
    providers: [RegionService]
})
export class RegionPageModule {
}
