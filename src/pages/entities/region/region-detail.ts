import { Component } from '@angular/core';
import { IonicPage, ModalController, NavParams, ToastController } from 'ionic-angular';
import { Region } from './region.model';
import { RegionService } from './region.provider';

@IonicPage({
    segment: 'region-detail/:id',
    defaultHistory: ['EntityPage', 'regionPage']
})
@Component({
    selector: 'page-region-detail',
    templateUrl: 'region-detail.html'
})
export class RegionDetailPage {
    region: Region;

    constructor(private modalCtrl: ModalController, params: NavParams,
                private regionService: RegionService, private toastCtrl: ToastController) {
        this.region = new Region();
        this.region.id = params.get('id');
    }

    ionViewDidLoad() {
        this.regionService.find(this.region.id).subscribe(data => this.region = data);
    }

    open(item: Region) {
        let modal = this.modalCtrl.create('RegionDialogPage', {item: item});
        modal.onDidDismiss(region => {
            if (region) {
                this.regionService.update(region).subscribe(data => {
                    this.region = data;
                    let toast = this.toastCtrl.create(
                        {message: 'Region updated successfully.', duration: 3000, position: 'middle'});
                    toast.present();
                }, (error) => console.error(error));
            }
        });
        modal.present();
    }

}
