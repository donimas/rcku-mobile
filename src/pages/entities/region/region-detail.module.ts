import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { RegionDetailPage } from './region-detail';
import { RegionService } from './region.provider';

@NgModule({
    declarations: [
        RegionDetailPage
    ],
    imports: [
        IonicPageModule.forChild(RegionDetailPage),
        TranslateModule.forChild()
    ],
    exports: [
        RegionDetailPage
    ],
    providers: [RegionService]
})
export class RegionDetailPageModule {
}
