import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import { Region } from './region.model';
import { RegionService } from './region.provider';

@IonicPage()
@Component({
    selector: 'page-region-dialog',
    templateUrl: 'region-dialog.html'
})
export class RegionDialogPage {

    region: Region;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController, public viewCtrl: ViewController, public toastCtrl: ToastController,
                formBuilder: FormBuilder, params: NavParams,
                private regionService: RegionService) {
        this.region = params.get('item');
        if (this.region && this.region.id) {
            this.regionService.find(this.region.id).subscribe(data => {
                this.region = data;
            });
        } else {
            this.region = new Region();
        }

        this.form = formBuilder.group({
            id: [params.get('item') ? this.region.id : null],
            code: [params.get('item') ? this.region.code : '',  Validators.required],
            ru: [params.get('item') ? this.region.ru : '',  Validators.required],
            kk: [params.get('item') ? this.region.kk : '',  Validators.required],
            en: [params.get('item') ? this.region.en : '', ],
            isDeleted: [params.get('item') ? this.region.isDeleted : 'false',  Validators.required],
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the region, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

}
