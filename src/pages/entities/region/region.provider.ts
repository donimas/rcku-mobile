import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

import { Region } from './region.model';

@Injectable()
export class RegionService {
    private resourceUrl = Api.API_URL.replace('api', 'api') + '/regions';

    constructor(private http: HttpClient) { }

    create(region: Region): Observable<Region> {
        return this.http.post(this.resourceUrl, region);
    }

    update(region: Region): Observable<Region> {
        return this.http.put(this.resourceUrl, region);
    }

    find(id: number): Observable<Region> {
        return this.http.get(`${this.resourceUrl}/${id}`);
    }

    query(req?: any): Observable<any> {
        return this.http.get(this.resourceUrl);
    }

    delete(id: number): Observable<any> {
        return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response', responseType: 'text' });
    }
}
