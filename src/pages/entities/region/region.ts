import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, ToastController } from 'ionic-angular';
import { Region } from './region.model';
import { RegionService } from './region.provider';

@IonicPage({
    defaultHistory: ['EntityPage']
})
@Component({
    selector: 'page-region',
    templateUrl: 'region.html'
})
export class RegionPage {
    regions: Region[];

    // todo: add pagination

    constructor(private navCtrl: NavController, private regionService: RegionService,
                private modalCtrl: ModalController, private toastCtrl: ToastController) {
        this.regions = [];
    }

    ionViewDidLoad() {
        this.loadAll();
    }

    loadAll(refresher?) {
        this.regionService.query().subscribe(
            (response) => {
                this.regions = response;
                if (typeof(refresher) !== 'undefined') {
                    refresher.complete();
                }
            },
            (error) => {
                console.error(error);
                let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
                toast.present();
            });
    }

    trackId(index: number, item: Region) {
        return item.id;
    }

    open(slidingItem: any, item: Region) {
        let modal = this.modalCtrl.create('RegionDialogPage', {item: item});
        modal.onDidDismiss(region => {
            if (region) {
                if (region.id) {
                    this.regionService.update(region).subscribe(data => {
                        this.loadAll();
                        let toast = this.toastCtrl.create(
                            {message: 'Region updated successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                        slidingItem.close();
                    }, (error) => console.error(error));
                } else {
                    this.regionService.create(region).subscribe(data => {
                        this.regions.push(data);
                        let toast = this.toastCtrl.create(
                            {message: 'Region added successfully.', duration: 3000, position: 'middle'});
                        toast.present();
                    }, (error) => console.error(error));
                }
            }
        });
        modal.present();
    }

    delete(region) {
        this.regionService.delete(region.id).subscribe(() => {
            let toast = this.toastCtrl.create(
                {message: 'Region deleted successfully.', duration: 3000, position: 'middle'});
            toast.present();
            this.loadAll();
        }, (error) => console.error(error));
    }

    detail(region: Region) {
        this.navCtrl.push('RegionDetailPage', {id: region.id});
    }
}
