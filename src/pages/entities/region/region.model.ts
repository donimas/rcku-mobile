import { BaseEntity } from './../../../models';

export class Region implements BaseEntity {
    constructor(
        public id?: number,
        public code?: string,
        public ru?: string,
        public kk?: string,
        public en?: string,
        public isDeleted?: boolean,
    ) {
        this.isDeleted = false;
    }
}
