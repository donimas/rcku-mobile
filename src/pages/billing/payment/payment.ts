import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, ToastController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {PaymentService} from './payment.provider';

@IonicPage({
  defaultHistory: ['PaymentPage']
})
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html'
})
export class PaymentPage implements OnInit {

  entities: Array<any> = [
      {name: 'Region', component: 'RegionPage'},
      {name: 'Property', component: 'PropertyPage'},
      /* jhipster-needle-add-entity-page - JHipster will add entity pages here */
  ];

  currentProperty: any;
  paymentHistory: any[];

  constructor(public nav: NavController,
              private storage: Storage,
              private toastCtrl: ToastController,
              private paymentService: PaymentService) {

  }

  ngOnInit() {
    console.log('this is history page');
    this.storage.get('currentProperty').then((val) => {
      this.currentProperty = val;
      console.log('property from storage -> ', this.currentProperty);
      this.paymentService.getPaymentHistory(this.currentProperty.personalAccount).subscribe(
        (data) => {
          console.log('history ->', data);
          this.paymentHistory = data;
        }, (error) => {
          console.log(error);
          let toast = this.toastCtrl.create({message: 'Failed to load payment history', duration: 2000, position: 'middle'});
          toast.present();
        }
      );
    });
  }

  ionViewWillLoad() {
    let page = location.hash.substring(location.hash.lastIndexOf('/') + 1);
    let urlParts = location.hash.split('/');
    page = page.charAt(0).toUpperCase() + page.substring(1) + 'Page';
    let destination;
    this.entities.forEach(entity => {
      if (entity.component === page) {
        destination = entity.component;
      }
    });
    if (destination) {
      this.nav.push(destination);
    } else if (urlParts.length === 5) {
      // convert from URL to page name: foo-detail to FooDetailPage
      const detailPage = this.urlToTitleCase(urlParts[3]) + 'Page';
      this.nav.push(detailPage, {id: urlParts[4]})
    }
  }

  private urlToTitleCase(str) {
    return str.replace(/(-|^)([^-]?)/g, (_, prep, letter) => {
      return (prep && '') + letter.toUpperCase();
    });
  }
}
