
export class BillingPayment {
    constructor(
        public date?: any,
        public bankKassa?: string,
        public sum?: number,
    ) {
    }
}
