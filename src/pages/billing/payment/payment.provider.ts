import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';

@Injectable()
export class PaymentService {
    private resourceUrl = Api.API_URL.replace('api', 'condo/api');

    constructor(private http: HttpClient) { }

    getPaymentHistory(personalAccount: string): Observable<any> {
      return this.http.get(`${this.resourceUrl}/billing/getPaymentHistory/${personalAccount}`);
    }
}
