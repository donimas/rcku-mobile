import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { NgJhipsterModule } from 'ng-jhipster';
import {PaymentPage} from "./payment";
import {PaymentService} from './payment.provider';

@NgModule({
  declarations: [
    PaymentPage
    /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
  ],
  imports: [
    IonicPageModule.forChild(PaymentPage),
    TranslateModule.forChild(),
    NgJhipsterModule.forRoot({
      alertAsToast: true,
      i18nEnabled: false
    })
  ],
  exports: [PaymentPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    PaymentService
  ]
})
export class PaymentPageModule {
}
