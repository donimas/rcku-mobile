import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController, NavParams, ToastController, ViewController } from 'ionic-angular';
import {IndicationService} from './indication.provider';
import {CounterReading} from './counter-reading.model';

@IonicPage()
@Component({
    selector: 'page-indication-dialog',
    templateUrl: 'indication-dialog.html'
})
export class IndicationDialogPage {

    counterReading: CounterReading;
    isReadyToSave: boolean;

    form: FormGroup;

    constructor(public navCtrl: NavController,
                public viewCtrl: ViewController,
                public toastCtrl: ToastController,
                formBuilder: FormBuilder,
                params: NavParams,
                private indicationService: IndicationService) {
        this.counterReading = new CounterReading();

        this.form = formBuilder.group({
            previousValue: [params.get('item') ? this.counterReading.previousValue : '',  Validators.required],
            currentValue: [params.get('item') ? this.counterReading.currentValue : '',  Validators.required]
        });

        // Watch the form for changes, and
        this.form.valueChanges.subscribe((v) => {
            this.isReadyToSave = this.form.valid;
        });

    }

    ionViewDidLoad() {
    }

    /**
     * The user cancelled, dismiss without sending data back.
     */
    cancel() {
        this.viewCtrl.dismiss();
    }

    /**
     * The user is done and wants to create the region, so return it
     * back to the presenter.
     */
    done() {
        if (!this.form.valid) { return; }
        this.viewCtrl.dismiss(this.form.value);
    }

    onError(error) {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
    }

}
