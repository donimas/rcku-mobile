import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {Observable} from 'rxjs/Observable';
import {Api} from '../../../providers/api/api';
import {HttpClient} from "@angular/common/http";

@IonicPage({
  defaultHistory: ['IndicationPage']
})
@Component({
  selector: 'page-indication',
  templateUrl: 'indication.html'
})
export class IndicationPage implements OnInit {

  private resourceUrl = Api.API_URL.replace('api', 'condo/api');

  currentProperty: any;
  devices: any[];

  constructor(public nav: NavController,
              private storage: Storage,
              private http: HttpClient,) {

  }

  ngOnInit() {
    this.storage.get('currentProperty').then((val) => {
      this.currentProperty = val;
      console.log('property from storage -> ', this.currentProperty);


      /*this.getPaymentHistory(this.currentProperty.personalAccount).subscribe(
        (data) => {
          console.log('history ->', data);
          this.paymentHistory = data;
        }, (error) => {
          console.log(error);
          let toast = this.toastCtrl.create({message: 'Failed to load payment history', duration: 2000, position: 'middle'});
          toast.present();
        }
      );*/


    });

    this.storage.get('currentBillingInfo').then((val) => {
      console.log('current billing info ->', val);

      this.devices = val.devices;
    });
  }

  getMeterHistory(personalAccount: string, deviceId: string): Observable<any> {
    return this.http.get(`${this.resourceUrl}/billing/meterHistory/${personalAccount}/${deviceId}`);
  }

  showHistory(deviceId: string) {
    console.log('let go to hustory', deviceId);
    this.nav.push('IndicationHistoryPage', {id: deviceId, personalAccount: this.currentProperty.personalAccount});
  }
}
