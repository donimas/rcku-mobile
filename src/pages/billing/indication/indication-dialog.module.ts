import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import {IndicationService} from './indication.provider';
import {IndicationDialogPage} from './indication-dialog';

@NgModule({
    declarations: [
      IndicationDialogPage
    ],
    imports: [
        IonicPageModule.forChild(IndicationDialogPage),
        TranslateModule.forChild()
    ],
    exports: [
        IndicationDialogPage
    ],
    providers: [
        IndicationService
    ]
})
export class IndicationDialogPageModule {
}
