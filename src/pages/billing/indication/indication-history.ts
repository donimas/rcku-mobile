import {Component, OnInit} from '@angular/core';
import {IonicPage, ModalController, NavParams, ToastController} from 'ionic-angular';
import {IndicationService} from './indication.provider';
import {Storage} from '@ionic/storage';
import {CounterReading} from './counter-reading.model';

@IonicPage({
    segment: 'indication-history/:id',
    defaultHistory: ['IndicationPage', 'indicationPage']
})
@Component({
    selector: 'page-indication-history',
    templateUrl: 'indication-history.html'
})
export class IndicationHistoryPage {

    indication: any;
    history: any[];
    currentProperty: any;

    constructor(
                params: NavParams,
                private indicationService: IndicationService,
                private storage: Storage,
                private modalCtrl: ModalController,
                public toastCtrl: ToastController,) {
        this.indication = {};
        this.indication.id = params.get('id');
        this.indication.personalAccount = params.get('personalAccount');
    }

    ionViewDidLoad() {
      this.storage.get('currentProperty').then((val) => {
        this.currentProperty = val;
      });
      console.log(this.indication);
      this.indicationService.getMeterHistory(this.indication.personalAccount, this.indication.id).subscribe(data => this.history = data);
    }

    openIndicationSendModal(slidingItem: any, item: any) {
      let modal = this.modalCtrl.create('IndicationDialogPage', {item: item});
      modal.onDidDismiss((counterReading: CounterReading) => {
        if (counterReading) {
          counterReading.counterCode = this.indication.id;
          console.log('counter reading ->', counterReading);
          this.indicationService.sendIndication(this.currentProperty.id, [counterReading]).subscribe(result => {
            // this.history.push(data);
            let message = '';
            if(result.success) {
              message = 'Indication sent successfully';
            } else {
              message = result.message;
            }
            let toast = this.toastCtrl.create(
              {message: message, duration: 3000, position: 'middle'});
            toast.present();
          }, (error) => console.error(error));
        }
      });
      modal.present();
    }

}
