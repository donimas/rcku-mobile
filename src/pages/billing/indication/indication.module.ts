import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { NgJhipsterModule } from 'ng-jhipster';
import {IndicationPage} from './indication';
import {IndicationService} from './indication.provider';

@NgModule({
  declarations: [
    IndicationPage
    /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
  ],
  imports: [
    IonicPageModule.forChild(IndicationPage),
    TranslateModule.forChild(),
    NgJhipsterModule.forRoot({
      alertAsToast: true,
      i18nEnabled: false
    })
  ],
  exports: [IndicationPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    IndicationService
  ]
})
export class IndicationPageModule {
}
