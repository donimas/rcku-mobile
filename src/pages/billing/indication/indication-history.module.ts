import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import {IndicationHistoryPage} from './indication-history';
import {IndicationService} from './indication.provider';

@NgModule({
    declarations: [
        IndicationHistoryPage
    ],
    imports: [
        IonicPageModule.forChild(IndicationHistoryPage),
        TranslateModule.forChild()
    ],
    exports: [
      IndicationHistoryPage
    ],
    providers: [IndicationService]
})
export class IndicationHistoryPageModule {
}
