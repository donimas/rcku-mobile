import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';
import {CounterReading} from './counter-reading.model';

@Injectable()
export class IndicationService {
    private resourceUrl = Api.API_URL.replace('api', 'condo/api');

    constructor(private http: HttpClient) { }

    getMeterHistory(personalAccount: string, deviceId: string): Observable<any> {
      return this.http.get(`${this.resourceUrl}/billing/meterHistory/${personalAccount}/${deviceId}`);
    }

    sendIndication(propertyId: number, listOfCounterReading: CounterReading[]): Observable<any> {
      return this.http.post(`${this.resourceUrl}/properties-ext/sendCounterReadings/${propertyId}`, listOfCounterReading);
    }
}
