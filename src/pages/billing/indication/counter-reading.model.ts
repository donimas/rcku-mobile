
export class CounterReading {
    constructor(
        public counterCode?: string,
        public previousValue?: string,
        public currentValue?: number,
    ) {
    }
}
