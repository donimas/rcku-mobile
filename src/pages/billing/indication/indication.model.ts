
export class BillingIndication {
    constructor(
        public date?: any,
        public readings?: number,
        public source?: string,
    ) {
    }
}
