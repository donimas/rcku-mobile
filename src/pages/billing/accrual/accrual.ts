import {Component, OnInit} from '@angular/core';
import {IonicPage, ModalController, NavController, ToastController} from 'ionic-angular';
import {Storage} from '@ionic/storage';
import {AccrualService} from './accrual.provider';

@IonicPage({
  defaultHistory: ['AccrualPage']
})
@Component({
  selector: 'page-accrual',
  templateUrl: 'accrual.html'
})
export class AccrualPage implements OnInit {

  currentProperty: any;
  billHistory: any[];

  constructor(public nav: NavController,
              private storage: Storage,
              private toastCtrl: ToastController,
              private accrualService: AccrualService) {

  }

  ngOnInit() {
    console.log('this is history page');
    this.storage.get('currentProperty').then((val) => {
      this.currentProperty = val;
      console.log('property from storage -> ', this.currentProperty);
      this.accrualService.getAccrualHistory(this.currentProperty.personalAccount).subscribe(
        (data) => {
          console.log('history ->', data);
          this.billHistory = data;
        }, (error) => {
          console.log(error);
          let toast = this.toastCtrl.create({message: 'Failed to load payment history', duration: 2000, position: 'middle'});
          toast.present();
        }
      );
    });
  }
}
