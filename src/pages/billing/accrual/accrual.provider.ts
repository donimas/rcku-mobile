import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Api } from '../../../providers/api/api';
import {BillingAccrual} from './accrual.model';

@Injectable()
export class AccrualService {
    private resourceUrl = Api.API_URL.replace('api', 'condo/api');

    constructor(private http: HttpClient) { }

    getAccrualHistory(personalAccount: string): Observable<any> {
      return this.http.get(`${this.resourceUrl}/billing/billHistory/${personalAccount}`);
    }
}
