
export class BillingAccrual {
    constructor(
        public date?: any,
        public accruedSum?: number,
        public adustmentSum?: number,
        public paymentSum?: number,
        public saldo?: number,
    ) {
    }
}
