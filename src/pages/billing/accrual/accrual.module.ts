import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { NgJhipsterModule } from 'ng-jhipster';
import {AccrualPage} from './accrual';
import {AccrualService} from './accrual.provider';

@NgModule({
  declarations: [
    AccrualPage
    /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
  ],
  imports: [
    IonicPageModule.forChild(AccrualPage),
    TranslateModule.forChild(),
    NgJhipsterModule.forRoot({
      alertAsToast: true,
      i18nEnabled: false
    })
  ],
  exports: [AccrualPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    AccrualService
  ]
})
export class AccrualPageModule {
}
