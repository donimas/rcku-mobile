import { Component, OnInit } from '@angular/core';
import {App, IonicPage, MenuController, ModalController, NavController, ToastController} from 'ionic-angular';
import { Principal } from '../../providers/auth/principal.service';
import { FirstRunPage } from '../pages';
import { LoginService } from '../../providers/login/login.service';
import {Property} from '../entities/property/property.model';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {Api} from "../../providers/api/api";
import { Storage } from '@ionic/storage';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  account: Account;

  private resourceUrl = Api.API_URL.replace('api', 'condo/api');
  properties: Property[];
  currentProperty: any;
  billingInfo: any;

  constructor(public navCtrl: NavController,
              private principal: Principal,
              private app: App,
              private loginService: LoginService,
              private menuController: MenuController,
              private http: HttpClient,
              private toastCtrl: ToastController,
              private modalCtrl: ModalController,
              private storage: Storage
  ) {
    this.menuController.enable(true);
  }

  ngOnInit() {
    this.principal.identity().then((account) => {

      console.log('account -> ', account);
      if (account === null) {
        this.app.getRootNavs()[0].setRoot(FirstRunPage);
      } else {
        this.account = account;
      }

      this.loadProperties();

    });
  }

  setCurrentProperty(property: any) {
    console.log('current property -> ', property);
    this.storage.set('currentProperty', property);

    this.findPropertyByAccountNumber(this.currentProperty.personalAccount, null).subscribe(
      (response) => {
        this.billingInfo = response;
        console.log('billing info ->', this.billingInfo);
        this.storage.set('currentBillingInfo', this.billingInfo);
      }, (error) => {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load saldo', duration: 2000, position: 'middle'});
        toast.present();
      }
    );
  }

  loadProperties(refresher?) {
    this.forCurrentUser().subscribe(
      (response) => {
        this.properties = response;
        console.log('properties -> ', this.properties);

        if(this.properties.length > 0) {
          this.currentProperty = this.properties[0];
          this.setCurrentProperty(this.currentProperty);
        }

        if (typeof(refresher) !== 'undefined') {
          refresher.complete();
        }
      },
      (error) => {
        console.error(error);
        let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
        toast.present();
      });
  }

  forCurrentUser(req?: any): Observable<any> {
    return this.http.get(`${this.resourceUrl}/properties-ext/forCurrentUser`);
  }

  addProperty(slidingItem: any, item: Property) {
    console.log('item -> ', item);
    let modal = this.modalCtrl.create('PropertyDialogPage', {item: item});
    modal.onDidDismiss(property => {
      if (property) {
        console.log('added property -> ', property);
        this.findPropertyByAccountNumber(property.personal_account, null).subscribe(
          (response: any) => {
            console.log(response);
            const requestedProperty = response;
            if(property.last_payment === requestedProperty.lastPayment) {
              console.log('payments are equal');

              requestedProperty.personalAccount = requestedProperty.account;
              this.createProperty(requestedProperty).subscribe(data => {
                this.properties.push(data);
                let toast = this.toastCtrl.create(
                  {message: 'Property added successfully.', duration: 3000, position: 'middle'});
                toast.present();
              }, (error) => console.error(error));

            } else {
              console.log('payments are not equal');
              let toast = this.toastCtrl.create({message: 'Invalid data', duration: 2000, position: 'middle'});
              toast.present();
            }
          }, (error) => {
            console.error(error);
            let toast = this.toastCtrl.create({message: 'Failed to load data', duration: 2000, position: 'middle'});
            toast.present();
          }
        );
      }
    });
    modal.present();
  }

  createProperty(property: Property): Observable<Property> {
    return this.http.post(`${this.resourceUrl}/properties`, property);
  }

  findPropertyByAccountNumber(accountNumber: string, iin?: string) {
    return this.http.get(`${this.resourceUrl}/billing/getAccountInfo/${accountNumber}/${iin}`);
  }

  isAuthenticated() {
    return this.principal.isAuthenticated();
  }

  logout() {
    this.loginService.logout();
    this.app.getRootNavs()[0].setRoot(FirstRunPage);
  }
}
