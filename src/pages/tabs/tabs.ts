import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController } from 'ionic-angular';

import {Tab1Root, Tab4Root} from '../pages';
import { Tab2Root } from '../pages';
import { Tab3Root } from '../pages';

@IonicPage({ priority: 'high', segment: 'tabs' })
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  /*tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;*/
  tabHome: any = Tab1Root;
  tabPayment: any = Tab2Root;
  tabIndication: any = Tab3Root;
  tabAccrual: any = Tab4Root;
  // tabApplication: any = Tab4Root;

  tab1Title = " ";
  tab2Title = " ";
  tab3Title = " ";
  tabHomeTitle = " ";
  tabAccrualTitle = " ";
  tabPaymentTitle = " ";
  tabIndicationTitle = " ";
  tabApplicationTitle = " ";

  constructor(public navCtrl: NavController, public translateService: TranslateService) {
    translateService.get(['TAB1_TITLE', 'TAB2_TITLE', 'TAB3_TITLE',
      'TAB_ACCRUAL_TITLE', 'TAB_PAYMENT_TITLE', 'TAB_INDICATION_TITLE', 'TAB_APPLICATION_TITLE']).subscribe(values => {
      this.tab1Title = values['TAB1_TITLE'];
      this.tab2Title = values['TAB2_TITLE'];
      this.tab3Title = values['TAB3_TITLE'];
      this.tabHomeTitle = values['TAB1_TITLE'];
      this.tabAccrualTitle = values['TAB_ACCRUAL_TITLE'];
      this.tabPaymentTitle = values['TAB_PAYMENT_TITLE'];
      this.tabIndicationTitle = values['TAB_INDICATION_TITLE'];
      this.tabApplicationTitle = values['TAB_APPLICATION_TITLE'];
    });
  }

  getSelectedIndex() {
    let page = location.hash.substring(location.hash.lastIndexOf('/') + 1);
    page = page.charAt(0).toUpperCase() + page.substring(1) + 'Page';
    // const tabs = [this.tab1Root, this.tab2Root, this.tab3Root];
    const tabs = [this.tabHome, this.tabPayment, this.tabIndication, this.tabAccrual];
    const index = tabs.indexOf(page);
    // const entitiesTab = tabs.indexOf(this.tab2Root);
    const entitiesTab = tabs.indexOf(this.tabPayment);
    if (location.hash.includes('entities')) {
      return entitiesTab;
    } else if (index > -1) {
      return index;
    }
  }
}
